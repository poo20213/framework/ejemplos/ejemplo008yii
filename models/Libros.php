<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "libros".
 *
 * @property int $id
 * @property string|null $nombre
 * @property string|null $contenido
 *
 * @property Autores[] $autors
 * @property Autores[] $autors0
 * @property Escribe $escribe
 * @property Leer[] $leers
 */
class Libros extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'libros';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nombre'], 'string', 'max' => 100],
            [['contenido'], 'string', 'max' => 500],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nombre' => 'Nombre',
            'contenido' => 'Contenido',
        ];
    }

    /**
     * Gets query for [[Autors]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getAutors()
    {
        return $this->hasMany(Autores::className(), ['id' => 'autor'])->viaTable('escribe', ['libro' => 'id']);
    }

    /**
     * Gets query for [[Autors0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getAutors0()
    {
        return $this->hasMany(Autores::className(), ['id' => 'autor'])->viaTable('leer', ['libro' => 'id']);
    }

    /**
     * Gets query for [[Escribe]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getEscribe()
    {
        return $this->hasOne(Escribe::className(), ['libro' => 'id']);
    }

    /**
     * Gets query for [[Leers]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getLeers()
    {
        return $this->hasMany(Leer::className(), ['libro' => 'id']);
    }
}
