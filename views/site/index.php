<?php

/* @var $this yii\web\View */
use yii\helpers\Html;
$this->title = 'Gestion de Libros';
?>
<div class="site-index">

    <div class="jumbotron text-center bg-transparent">
        <h1 class="display-4">Ejemplo de aplicacion</h1>

        <p class="lead">Gestion de libros y autores</p>
    </div>

    <div class="body-content">

        <div class="row">
            <div class="col-lg-10 mx-auto">
                <?= Html::img('@web/imgs/1.png',['alt'=> 'Diseño','class'=> 'img-fluid']) ?>
            </div>
        </div>

    </div>
</div>
